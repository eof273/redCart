# :star: Red Cart Shop :star:

A small single page app relating to a "Shopping cart";

Is able to list the product and product categories and add items to the cart and save it for later checkout;

Is able to view the cart and delete / add more products / change the quantity / calculate the total price;

Can calculate in various currency;

#### Used technologies

* [ES6](http://es6-features.org/#Constants)
* [Webpack2](https://webpack.js.org/configuration/)
* [npm](https://docs.npmjs.com/)
* [Babel](https://babeljs.io/)
* [Bootstrap](http://getbootstrap.com/)
* [Angular 1.6.4](https://angularjs.org/)
    * [@uirouter/angularjs](https://github.com/angular-ui/ui-router)
    * [angular-local-storage](https://github.com/grevory/angular-local-storage)
    * [angular-ui-notification](https://github.com/alexcrack/angular-ui-notification)

#### Install
Use [npm](https://www.npmjs.com/):
```sh
npm install
```
or [yarn](https://yarnpkg.com/):
```sh
yarn install
```

```sh
npm install webpack-dev-server -g
webpack-dev-server
```