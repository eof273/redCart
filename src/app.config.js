/*@ngInject*/
export default (localStorageServiceProvider, $locationProvider, $urlRouterProvider, $stateProvider, NotificationProvider) => {
  localStorageServiceProvider.setPrefix('app');

  $locationProvider.html5Mode(true);

  $urlRouterProvider.when('/', '/goods/0');
  $urlRouterProvider.otherwise('/error/404');

  NotificationProvider.setOptions({
    delay: 3000,
    positionY: 'bottom'
  });
};
