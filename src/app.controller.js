class AppController {
  /*@ngInject*/
  constructor(categoriesService, coursesService, $state, $stateParams, cartService) {
    this.$categoriesService = categoriesService;
    this.$coursesService = coursesService;
    this.$cartService = cartService;
    this.$state = $state;
    this.$stateParams = $stateParams;


    this.categories = this.$categoriesService.getCategories();
    this.courses = this.$coursesService.getCourses();
    this.mainCourse = this.courses[0];
  }

  getCartCost() {
    let cart = this.$cartService.getCart();
    let cartCost = 0;
    if (cart.length > 0) {
      cartCost = cart.reduce((summ, product) => summ + (+product.price * product.count), 0);
      cartCost = this.$coursesService.getPrice(cartCost);
    }
    return cartCost;
  }

  changeCurrency(cr) {
    this.$coursesService.setMainCurrency(cr);
  }

  getCurrencySymbol() {
    return this.$coursesService.getCurrencySymbol();
  }

  isActiveTab(state, category) {
    let result = false;

    if (category === undefined && this.$state.$current.name === state) {
      result = true;
    }

    if (this.$state.$current.name === state && Number(this.$stateParams.category) === category) {
      result = true;
    }

    return result;
  }
}

export default AppController;
