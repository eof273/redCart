/*@ngInject*/
export default ($stateProvider) => {
  $stateProvider
    .state('goods', {
      url: '/goods/:category',
      component: 'goods'
    });
};
