class GoodsController {
  /*@ngInject*/
  constructor(categoriesService, goodsService, cartService, coursesService, localStorageService, $state, $stateParams) {
    this.$categoriesService = categoriesService;
    this.$goodsService = goodsService;
    this.$cartService = cartService;
    this.$coursesService = coursesService;
    this.$localStorage = localStorageService;
    this.$stateParams = $stateParams;
    this.$state = $state;

    this.category = this.$categoriesService.getCategoryById(+this.$stateParams.category);

    // redirect to 404 if category not exists
    if (!this.category) {
      this.$state.go('error', {
        errorCode: 404
      });
    }

    this.goods = this.$goodsService.getGoods(Number(this.$stateParams.category));
  }

  addToCart(prod) {
    this.$cartService.addToCart(prod);
  }

  countMinus(prod) {
    if (prod.count > 1)
      prod.count--;
  }

  countPlus(prod) {
    if (prod.count < prod.maxCount)
      prod.count++;
  }

  getPrice(price) {
    return this.$coursesService.getPrice(price);
  }

  getTotalPrice(product) {
    return this.getPrice(product.price * product.count);
  }

  getCurrencySymbol() {
    return this.$coursesService.getCurrencySymbol();
  }

}

export default GoodsController;
