import goods from '../../db/goods.json';

class GoodsService {
  /*@ngInject*/
  constructor(localStorageService) {
    this.goods = goods;
    this.$localStorage = localStorageService;
  }

  getGoods(categoryId) {
    return goods.filter(product => product.category === categoryId);
  }
}

export default GoodsService;
