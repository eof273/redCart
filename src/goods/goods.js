import goodsComponent from './goods.component';
import goodsRoutes from './goods.routes';

export default angular.module('app.goods', [])
  .component('goods', goodsComponent)
  .config(goodsRoutes)
  .name;
