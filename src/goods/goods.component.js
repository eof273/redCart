import controller from './goods.controller';
import template from './goods.template.html';

const goodsComponent = {
  controller,
  controllerAs: '$goodsCtrl',
  template
};

export default goodsComponent;
