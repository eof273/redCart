/*@ngInject*/
export default ($stateProvider) => {
  $stateProvider.state('app', {
    url: '/',
    component: 'appComponent'
  });
};