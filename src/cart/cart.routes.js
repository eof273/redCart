/*@ngInject*/
export default ($stateProvider) => {
  $stateProvider
    .state('cart', {
      url: '/cart',
      component: 'cart'
    });
};
