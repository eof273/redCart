class CartController {
  /*@ngInject*/
  constructor(cartService, localStorageService, coursesService) {
    this.$cartService = cartService;
    this.$localStorage = localStorageService;
    this.$coursesService = coursesService;

    this.cart = this.$cartService.getCart();
    this.cost = this.getCost();
  }

  deleteFromCart(prod) {
    if (confirm('Do you really want to delete that product?')) {
      this.$cartService.deleteFromCart(prod);
      this.cart = this.$cartService.getCart();
      this.cost = this.getCost();
    }
  }

  getCost() {
    let result = 0;

    if (this.cart.length > 0) {
      result = this.cart.reduce((summ, product) => summ + (+product.price * product.count), 0);
    }

    return result;
  }

  countMinus(prod) {
    if (prod.count > 1) {
      prod.count--;
      this.cost = this.getCost();
      this.$cartService.saveCart(this.cart);
    }
  }

  countPlus(prod) {
    if (prod.count < prod.maxCount) {
      prod.count++;
      this.cost = this.getCost();
      this.$cartService.saveCart(this.cart);
    }
  }

  /**
   * Data processing, for data changed from text input;
   * @param {type} prod - element from "goods.json";
   */
  changedCount(prod) {
    prod.count = +prod.count;
    if (!prod.count) {
      //Can't set less than 0;
      prod.count = 1;
    } else if (prod.count > prod.maxCount) {
      //Return old value for invalid data;
      let oldCart = this.$cartService.getCart();
      let oldProd = oldCart.find((oProd) => {
        return oProd.id === prod.id;
      });

      prod.count = oldProd.count;
    } else {
      this.cost = this.getCost();
      this.$cartService.saveCart(this.cart);
    }
  }

  getPrice(price) {
    return this.$coursesService.getPrice(price);
  }

  getTotalPrice(product) {
    return this.getPrice(product.price * product.count);
  }

  getCurrencySymbol() {
    return this.$coursesService.getCurrencySymbol();
  }

  saveChanges() {
    //goto pay;
  }
}

export default CartController;
