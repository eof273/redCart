import cartComponent from './cart.component';
import cartRoutes from './cart.routes';

export default angular.module('app.cart', [])
  .component('cart', cartComponent)
  .config(cartRoutes)
  .name;
