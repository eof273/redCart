class CartService {
  /*@ngInject*/
  constructor(localStorageService, Notification, $state) {
    this.$localStorage = localStorageService;
    this.$Notification = Notification;
    this.$state = $state;
    this.cart = this.getCart();
  }

  /**
   * Add product to cart;
   * @param {type} prod - element from "goods.json"
   * @returns {Boolean} - information about operation success;
   */
  addToCart(prod) {
    let cart = this.$localStorage.get('cart') || [];
    let prodInCart = cart.find((product) => product.id === prod.id);

    if (prodInCart) {

      if (prodInCart.count + prod.count > prod.maxCount) {
        this.$Notification.error('To much!');
        this.$state.go('additional');
        return false;
      } else {
        prodInCart.count += prod.count;
        this.saveCart(cart);
        this.cart = this.getCart();
        this.$Notification.success('Add to prod. in cart');
      }

    } else {

      if (prod.count > prod.maxCount) {
        this.$Notification.error('To much!');
        this.$state.go('additional');
        return false;
      } else {
        cart.push(prod);
        this.saveCart(cart);
        this.cart = this.getCart();
        this.$Notification.success('Add new to cart');
      }
    }
    return true;
  }

  deleteFromCart(prod) {
    this.cart.splice(this.cart.findIndex((product) => product.id === prod.id), 1);
    this.saveCart();
  }

  getCart() {
    return this.$localStorage.get('cart') || [];
  }

  saveCart(cart) {
    this.$localStorage.set('cart', cart || this.cart);
  }
}

export default CartService;
