import controller from './cart.controller';
import template from './cart.template.html';

const cartComponent = {
  controller,
  controllerAs: '$cartCtrl',
  template
};

export default cartComponent;
