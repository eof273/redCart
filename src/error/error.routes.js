/*@ngInject*/
export default ($stateProvider) => {
  $stateProvider
    .state('error', {
      url: '/error/:errorCode',
      component: 'error'
    });
};
