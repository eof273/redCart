import controller from './error.controller';
import template from './error.template.html';

const errorComponent = {
  controller,
  controllerAs: '$errorCtrl',
  template
};

export default errorComponent;
