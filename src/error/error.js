import errorComponent from './error.component';
import errorRoutes from './error.routes';

export default angular.module('app.error', [])
  .component('error', errorComponent)
  .config(errorRoutes)
  .name;
