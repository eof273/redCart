class errorController {
  /*@ngInject*/
  constructor($stateParams) {
    this.$stateParams = $stateParams;

    this.error = $stateParams.errorCode;
  }
}

export default errorController;
