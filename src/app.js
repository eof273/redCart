import angular from 'angular';
import angularAnimate from 'angular-animate';
import angularUiBootstrap from 'angular-ui-bootstrap';
import localStorageModule from 'angular-local-storage';
import uiRouter from '@uirouter/angularjs';
import angularUiNotification from 'angular-ui-notification';

import appServices from './app.services';
import './sass/app.scss';

import goods from './goods/goods';
import cart from './cart/cart';
import about from './about/about';
import additional from './additional/additional';
import error from './error/error';

import appComponent from './app.component';
import appConfig from './app.config';

angular.module('app', [
  angularAnimate, angularUiBootstrap, localStorageModule, uiRouter, angularUiNotification,
  appServices,
  goods, cart, about, additional, error
]).component('appComponent', appComponent)
  .config(appConfig);
