import categories from '../../db/categories.json';

class CategoriesService {
  constructor() {
    this.categories = this.getCategories();
  }

  getCategories() {
    return categories;
  }

  getCategoryById(id) {
    return categories.find((category) => category.id === id);
  }

}

export default CategoriesService;
