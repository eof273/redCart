import controller from './app.controller';
import template from './app.html';

const appComponent = {
  controller,
  controllerAs: '$appCtrl',
  template
};

export default appComponent;
