class AdditionalController {
  /*@ngInject*/
  constructor($state, Notification) {
    this.$state = $state;
    this.$Notification = Notification;
  }

  toCart() {
    this.$state.go('cart');
  }

  acceptRationCard() {
    //logic for real ration card number;
    this.$Notification.error('Incorrect number!');
  }

}

export default AdditionalController;
