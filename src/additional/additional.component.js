import controller from './additional.controller';
import template from './additional.template.html';

const additionalComponent = {
  controller,
  controllerAs: '$additionalCtrl',
  template
};

export default additionalComponent;
