import additionalComponent from './additional.component';
import additionalRoutes from './additional.routes';

export default angular.module('app.additional', [])
  .component('additional', additionalComponent)
  .config(additionalRoutes)
  .name;
