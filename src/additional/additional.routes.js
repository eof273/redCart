/*@ngInject*/
export default ($stateProvider) => {
  $stateProvider
    .state('additional', {
      url: '/additional',
      component: 'additional'
    });
};
