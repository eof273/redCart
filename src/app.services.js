import categoriesService from './categories/categories.service';
import goodsService from './goods/goods.service';
import cartService from './cart/cart.service';
import coursesService from './courses/courses.service';

export default angular.module('app.services', [])
  .service('categoriesService', categoriesService)
  .service('goodsService', goodsService)
  .service('cartService', cartService)
  .service('coursesService', coursesService)
  .name;
