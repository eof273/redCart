import courses from '../../db/courses.json';

class CoursesService {
  constructor() {
    this.courses = this.getCourses();
    this.mainCourse = this.courses[0];
  }

  getCourses() {
    return courses;
  }

  setMainCurrency(cr) {
    this.mainCourse = cr;
  }

  getPrice(price) {
    return Math.round((price / this.mainCourse.Value) * 100) / 100;
  }

  getCurrencySymbol() {
    return this.mainCourse.Symbol;
  }

}

export default CoursesService;
