/*@ngInject*/
export default ($stateProvider) => {
  $stateProvider
    .state('about', {
      url: '/about',
      component: 'about'
    });
};
