import aboutComponent from './about.component';
import aboutRoutes from './about.routes';

export default angular.module('app.about', [])
  .component('about', aboutComponent)
  .config(aboutRoutes)
  .name;
