const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: './src/app.js',

  output: {
    filename: '[hash].dist.js',
    path: path.join(__dirname, 'dist')
  },

  resolve: {
    modules: ['node_modules'],
    extensions: ['.json', '.js', '.jsx', '.css']
  },

  devtool: 'source-map',
  module: {
    rules: [{
        test: /\.(jpg|png|svg)$/,
        loader: 'url-loader',
        options: {
          limit: 25000,
        },
      }, {
        test: /\.js$/,
        exclude: [
          path.join(__dirname, 'node_modules')
        ],
        use: [{
            loader: 'ng-annotate-loader'
          }, {
            loader: 'babel-loader'
          }]
      }, {
        test: /\.html$/,
        loader: 'html-loader',
        options: {
          interpolate: true
        }
      }, {
        test: /\.s?css$/,
        use: [
          {
            loader: 'style-loader'
          }, {
            loader: 'css-loader', options: {
              importLoaders: 1
            }
          }, {
            loader: 'postcss-loader',
            options: {
              ident: 'postcss',
              plugins: () => {
                return [
                  require('autoprefixer')({
                    browsers: [
                      "> 5%",
                      "last 5 versions"
                    ]
                  })
                ];
              },
              sourceMap: true
            }
          },
          {
            loader: 'sass-loader', options: {
              sourceMap: true,
              unixNewlines: true,
              precision: 10
            }
          }]
      }]
  },

  plugins: [
    new HtmlWebpackPlugin({
      template: './src/index.html'
    })
  ],

  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    host: '127.0.0.1',
    watchContentBase: true,
    historyApiFallback: true
  }
};